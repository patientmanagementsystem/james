/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem;

import patientmanagementsystem.controller.AddPatientController;
import patientmanagementsystem.controller.DashboardController;
import patientmanagementsystem.controller.PatientEntryController;
import patientmanagementsystem.model.PatientMysql;
import patientmanagementsystem.view.AddPatient;
import patientmanagementsystem.model.PatientTableModel;
import patientmanagementsystem.view.Dashboard;
import patientmanagementsystem.view.PatientTable;

/**
 *
 * @author James.Mugo
 */
public class PatientManagementSystem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        PatientMysql psql = new PatientMysql();
        
        Dashboard patientDashboard = new Dashboard();
        AddPatient patient = new AddPatient(); 
        PatientTable tableView = new PatientTable();
        PatientTableModel model = PatientTableModel.getInstance();
        
        patientDashboard.setVisible(true); 
        
        AddPatientController pcontrol = new AddPatientController(patient,model);
        pcontrol.control();
        
        PatientEntryController pEntry = new PatientEntryController(tableView, model);
        pEntry.control(); 
        
        DashboardController dc = new  DashboardController(patientDashboard, patient, tableView);
        dc.control();
    }
    
}
