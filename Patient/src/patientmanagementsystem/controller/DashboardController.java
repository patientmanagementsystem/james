/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import patientmanagementsystem.model.Patient;
import patientmanagementsystem.model.PatientTableModel;
import patientmanagementsystem.view.AddPatient;
import patientmanagementsystem.view.Dashboard;
import patientmanagementsystem.view.PatientTable;

/**
 *
 * @author James.Mugo
 */
public class DashboardController implements ActionListener{
    Dashboard dash;
    PatientTable pTable;
    AddPatient addPatient;
    Patient patient;
    PatientTableModel ptModel;
    AddPatientController addPCon;
    PatientEntryController pFormCon;
    
    /**
     * constructor to set default values
     * @param d an object of dashboard form which is the central control
     * of the system
     * @param pat the patient form which is used to add and update 
     * patient records in the database
     * @param pT the patient table which is a tabular view of the patients
     * stored in the database. 
     */
    public DashboardController(Dashboard d, AddPatient pat, PatientTable pT){
        this.dash = d;
        this.addPatient = pat;
        this.pTable = pT;
        this.patient = new Patient();
    }
    
    public void control(){
        dash.getAddButton().addActionListener(this); 
        dash.getDeleteButton().addActionListener(this);
        dash.getUpdateButton().addActionListener(this);
        dash.getViewButton().addActionListener(this);
        dash.getSearchButton().addActionListener(this);
        
        dash.getItemAddPatient().addActionListener(this);
        dash.getItemUpdatePatient().addActionListener(this);
        dash.getItemDeletePatient().addActionListener(this);
        dash.getItemSearchPatient().addActionListener(this);
        dash.getItemViewPatient().addActionListener(this); 
        dash.getItemImport().addActionListener(this);
        dash.getItemExport().addActionListener(this);
        dash.getItemLogout().addActionListener(this); 
        dash.getItemHelp().addActionListener(this); 
        
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource()== dash.getAddButton() ||
            ae.getSource() == dash.getItemAddPatient())
        {    
            System.out.println("Action listener 1");
            addPatient.setVisible(true);
        }
        else if (ae.getSource()== dash.getUpdateButton() ||
                 ae.getSource()==dash.getItemUpdatePatient())
        {    
            System.out.println("Action listener 2 -Update called");
            addPatient.setVisible(true);
        }
        else if (ae.getSource()==   dash.getViewButton() ||
                 ae.getSource() == dash.getItemViewPatient())
        {   
            this.pTable = new PatientTable();
            this.ptModel = new PatientTableModel();
            this.pFormCon = new PatientEntryController(pTable, ptModel);
            System.out.println(" Action Listener 2");
            pTable.setVisible(true);
            pFormCon.control();
        }
        else if (ae.getSource()==dash.getDeleteButton() ||
                 ae.getSource()==dash.getItemDeletePatient())   
        {   
            try{
            String key = JOptionPane.showInputDialog(dash, " Enter the ID of the patient to delete ");
            System.out.println(" The key is " + key);
            ptModel.delete(key);
            JOptionPane.showMessageDialog(dash, " You have succefully deleted a record");
            }catch(Exception ex){
                System.out.println("Error: " + ex.toString());
            }
        }
        else if(ae.getSource() == dash.getSearchButton() ||
                ae.getSource() == dash.getItemSearchPatient()){
            try{
            String key = JOptionPane.showInputDialog(dash, " Enter the ID of the patient to search ");
            System.out.println(" The key is " + key);
            ptModel.search(key);
            
            }catch(Exception ex){
                JOptionPane.showMessageDialog(dash,"There is no such patient");
            }
        }
        else if(ae.getSource() == dash.getItemImport()){
            try{
            ptModel.importData();
            }catch(Exception ex){
                ex.toString();
            }
        }
        else if(ae.getSource() == dash.getItemExport()){
            try{
               ptModel.exportData(); 
            }catch(Exception ex){
                ex.toString();
            }
            
        }
        else if (ae.getSource() == dash.getItemLogout() )
        {               
           System.exit(0);
        }
        else if (ae.getSource() == dash.getItemHelp()){
            JOptionPane.showMessageDialog(dash, "Click on file for:\n1. Add patient"
                    + "\n2. update a patient record\n3. view the patients admitted so far"
                    + "\n4. search a patient\n\nClick on user for:\n*logout and help","Help",3);
        
        }
    }
}
