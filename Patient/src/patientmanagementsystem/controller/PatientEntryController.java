/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import patientmanagementsystem.model.PatientTableModel;
import patientmanagementsystem.view.AddPatient;
import patientmanagementsystem.view.Dashboard;
import patientmanagementsystem.view.PatientTable; 

/**
 *
 * @author James.Mugo
 */
public class PatientEntryController implements ActionListener{
    AddPatient form;
    PatientTableModel model;
    AddPatientController patientControl;
    PatientTable pTable;
    Dashboard dashboard;
    
    /**
     * constructor that sets the forms
     * @param pTable the patient table 
     * @param model the patient table model
     */
    public PatientEntryController(PatientTable pTable,PatientTableModel model){
        this.pTable = pTable;
        this.model = model; 
        this.form = new AddPatient(); 
        this.patientControl = new AddPatientController(form, model);
        pTable.getPatientTable().setModel(model); 
    }
    public void control(){
        pTable.getAddButton().addActionListener(this); 
        pTable.getUpdateButton().addActionListener(this);
        pTable.getDeleteButton().addActionListener(this);
        pTable.getExitButton().addActionListener(this);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==pTable.getAddButton()){
            form.setVisible(true);
            patientControl.control();
        }
        if(e.getSource()==pTable.getUpdateButton()){
            form.setVisible(true); 
            patientControl.control();
        }
        if(e.getSource()==pTable.getDeleteButton()){
            String id = JOptionPane.showInputDialog(null, " Enter the ID of the student to delete ");
            System.out.println(" The key is " + id); 
            model.delete(id); 
        }
        if(e.getSource()==pTable.getExitButton()){
            pTable.dispose();
            try{
            dashboard.setVisible(true);
            }catch(Exception ex){
                System.out.println("Error:" + ex.toString());
            }
            //Login log = new Login();
            //log.setVisible(true); 
        }
    }
}
