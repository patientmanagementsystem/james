/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import patientmanagementsystem.view.AddPatient;
import patientmanagementsystem.model.PatientTableModel;

/**
 *
 * @author James.Mugo
 */
public class AddPatientController implements ActionListener{
    AddPatient view;
    PatientTableModel model;
    
    /**
     * constructor to set the view and model
     * @param addNewPatient the new patient entry form
     * @param tableOfPatients the model for the patient table
     */
    public AddPatientController(AddPatient addNewPatient, PatientTableModel tableOfPatients){
        view = addNewPatient;
        model = tableOfPatients;
    }
    public void control(){
        view.getAddButton().addActionListener(this);
        view.getExitButton().addActionListener(this);
        view.getUpdateButton().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==view.getExitButton()){
            view.dispose();
            try{ 
            }catch(Exception ex){
                System.out.println("Error: " + ex.toString());
            }
        }
        else if(e.getSource()==view.getAddButton()){  
         System.out.println("currently in add");
         System.out.println(view.getPatientID() + view.getSurName()+view.getFirstName()+
         view.getAddress()+ view.getPhone()+ view.getNextOfKin() +view.getNextContact());
         
        String id = null, firstname = null, surname = null, phone=null, address= null, next=null, cont=null;
        boolean allTrue = true;
            
        if("".equals(view.getPatientID()) && "".equals(view.getSurName()) && "".equals(view.getFirstName())
        && "".equals(view.getAddress()) && "".equals(view.getPhone()) && "".equals(view.getNextOfKin()) && 
        "".equals(view.getNextContact()) ){
            JOptionPane.showMessageDialog(view, " Please fill in all the details");
            allTrue = false;
        }
        else
        {
            System.out.println("currently adding");
            //view.setVisible(true); 
            id = view.getPatientID();
            surname= view.getSurName();
            firstname=view.getFirstName();
            address = view.getAddress();
            phone = view.getPhone();
            next = view.getNextOfKin();
            cont = view.getNextContact();
        }
            
            if (allTrue == true){ 
                model.insert(id,surname,firstname,address, phone, next, cont); 
                JOptionPane.showMessageDialog(view, " Have successfully entered a new entry");
                model.fireTableDataChanged();
            }
        }
        else if(e.getSource()==view.getUpdateButton()){  
         System.out.println("currently in update");
         System.out.println(view.getPatientID() + view.getSurName()+view.getFirstName()+
         view.getAddress()+ view.getPhone()+ view.getNextOfKin() +view.getNextContact());
         
        String id = null, firstname = null, surname = null, phone=null, address= null, next=null, cont=null;
        boolean allTrue = true;
            
        if("".equals(view.getPatientID()) && "".equals(view.getSurName()) && "".equals(view.getFirstName())
        && "".equals(view.getAddress()) && "".equals(view.getPhone()) && "".equals(view.getNextOfKin()) && 
        "".equals(view.getNextContact()) ){
            JOptionPane.showMessageDialog(view, " Please fill in all the details");
            allTrue = false;
        }
        else
        {
            System.out.println("currently updating");
            //view.setVisible(true); 
            id = view.getPatientID();
            surname= view.getSurName();
            firstname=view.getFirstName();
            address = view.getAddress();
            phone = view.getPhone();
            next = view.getNextOfKin();
            cont = view.getNextContact();
        }
            
            if (allTrue == true){ 
                model.update(id,surname,firstname,address, phone, next, cont); 
                JOptionPane.showMessageDialog(view, " Have successfully updated a new entry");
                model.fireTableDataChanged();
            }
        }
        
    }
    
}
