/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.model;

/**
 *
 * @author James.Mugo
 */
public class Patient {
    /*instance variabkes*/
    String patientID;
    String surName;
    String firstName;
    String address;
    String phone;
    String nextOfKin;
    String contact;
    
    /*mutator and accessor methods*/
    public void setPatient(String id){
        patientID = id;
    }
    public void setSurname(String sur){
        surName = sur;
    }
    public void setFirstname(String first){
        firstName = first;
    }
    public void setAddress(String add){
        address = add;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public void setNextOfKin(String kin){
        nextOfKin = kin;
    }
    public void setContact(String cont){
        contact = cont;
    }
    public String getPatientID(){
        return patientID;
    }
    public String getSurname(){
        return surName;
    }
    public String getFirstName(){
        return firstName;
    }
    public String getAddress(){
        return address;
    }
    public String getPhone(){
        return phone;
    }
    public String getNextOfKin(){
        return nextOfKin;
    }
    public String getContact(){
        return contact;
    }
}
