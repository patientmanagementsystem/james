/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author James.Mugo
 * a class that connects to the database and has methods for various
 * operations such as insert() and delete
 */
public class PatientTableModel extends AbstractTableModel{
    /*an arrayList to store the records of a table*/
    ArrayList<Patient> staffArray = new ArrayList<Patient>();
    static PatientTableModel theModel;
    String[] columnNames=null; 
    String[] keys;
    
    Connection conn = null;
    /**
     * constructor that sets the initial table values
     */
    public PatientTableModel(){
     super();
    fetchTableData();
    }
    /*Singleton pattern to instantiate a table*/
    public static PatientTableModel getInstance(){     
        if (theModel==null)
            theModel= new PatientTableModel();
        return theModel;
    }
    

    @Override
    /**
     * a method that returns the size of the arrayList depending on the
     * user's population to the table
     * @return size of the arrayList
     */
    public int getRowCount() {
        return staffArray.size();
    }

    @Override
    /**
     * method to get the number of columns in a table
     * @return integer number 
     */
    public int getColumnCount() {
        return 7;
    }
    
    @Override
    /**
     * a method that fetches a value at a given column and row
     * @param rowIndex the index of the row 
     * @param columnIndex the index of the column 
     * @retun object of the table
     */
    public Object getValueAt(int rowIndex, int columnIndex) {
                    Object obj = null;
         switch (columnIndex){
                case 0:
             obj = staffArray.get(rowIndex).patientID;
             break;
          case 1:
             obj = staffArray.get(rowIndex).surName;
             break;
          case 2:
             obj = staffArray.get(rowIndex).firstName;
             break;
          case 3:
             obj = staffArray.get(rowIndex).address;
             break;
          case 4:
              obj =staffArray.get(rowIndex).phone;
              break;
          case 5:
              obj = staffArray.get(rowIndex).nextOfKin; 
              break;
          case 6:
              obj = staffArray.get(rowIndex).contact;
        }
        return obj; 
    }
    
    /**
     * a method that gets the column name given the index of the column
     * @param col the integer value for the column
     * @return column name represented by the given index
     */
    public String getColumnName(int col){
        String colName = null;
        switch(col){
            case 0:
                colName = "PATIENT ID";
                break;
            case 1:
                colName = "SUR NAME";
                break;
            case 2:
                colName = "FIRST NAME";
                break;
            case 3:
                colName = "ADDRESS";
                break;
            case 4:
                colName = "PHONE";
                break;
            case 5:
                colName = "NEXT OF KIN";
                break;
            case 6:
                colName = "CONTACT";
                break;
        }
        return colName;
    }
    
    /**
     * a method that fetches the entire table and its content
     */
    public void fetchTableData(){
            try { 
                Connection conn = null; 
                Class.forName("com.mysql.jdbc.Driver").newInstance(); 
                conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/hospital?user=root&password="); 
                Statement s = conn.createStatement(); 
                ResultSet rs = s.executeQuery("SELECT * FROM patient"); 
                ResultSetMetaData meta = rs.getMetaData(); 
                int numberOfColumns = meta.getColumnCount(); 
                columnNames= new String[numberOfColumns]; 
                    for (int i = 1; i <= numberOfColumns; i++) {  
                        columnNames[i - 1] = meta.getColumnName(i);  
                    } 
                    
                while(rs.next()) {  
                    Patient newPatient = new Patient();
                    newPatient.setPatient(rs.getString(1));
                    newPatient.setSurname(rs.getString(2));
                    newPatient.setFirstname(rs.getString(3));
                    newPatient.setAddress(rs.getString(4));
                    newPatient.setPhone(rs.getString(5));
                    newPatient.setNextOfKin(rs.getString(6)); 
                    newPatient.setContact(rs.getString(7)); 
 
                    staffArray.add(newPatient); 
                    }  
                } 
            catch (Exception e) { 
                e.printStackTrace(); 
                System.out.println(e); 
                System.exit(0); 
            }
        }
    
    /**
     * a method that searches the database and displays the results 
     * @param id the identity of the patient
     */
    public void search(String id){
        System.out.println("searching ... ");
        System.out.println(" The key is " + id);
        
        
        try{  
            Connection conn = null; 
            Class.forName("com.mysql.jdbc.Driver").newInstance(); 
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/hospital?user=root&password="); 
             
            PreparedStatement p = conn.prepareStatement("SELECT * FROM patient WHERE patientID=?");
            p.setString(1,id);
     
            ResultSet q=p.executeQuery();
          
            if(q.next()) 
                JOptionPane.showMessageDialog(null,"The patient with Patient Number "+ id+" is "+q.getString("surName"));
            else
                JOptionPane.showMessageDialog(null,"There is no such patient");
           }
          catch (Exception e){
              System.out.println(e);
                // System.exit(0);
          }
    }

    /**
     * a method that updates the database with new information as entered
     * by the user
     * @param id the patient identity as given to the user 
     * @param sur the sur name of the patient
     * @param firs the first name of the patient
     * @param add the address of the patient
     * @param phone the phone number of the patient
     * @param next the next of Kin to the patient
     * @param cont the contact of the next of kin
     */
    public void update(String id, String sur, String firs, String add, String phone,String next,String cont){  
        try{
        Connection conn = null; 
        Class.forName("com.mysql.jdbc.Driver").newInstance(); 
        conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/hospital?user=root&password=");
                
        PreparedStatement p =conn.prepareStatement("update patient set surName = ?, firstName = ?, phone = ?,"
                            + "address = ?, nextOfKin = ?, contact = ? where patientID = ?");
        
        p.setString(1,sur);
        p.setString(2,firs);
        p.setString(3, add); 
        p.setString(4, phone);
        p.setString(5, next);
        p.setString(6, cont); 
        p.setString(7,id);
                    
        int x= p.executeUpdate(); //use execute if no results expected back
        System.out.println("Number of rows affected is "+ x);
        p.close();
    }
    catch(Exception e){
        System.out.println("Error"+e.toString());
    }
  }
    /**
     * a method that deletes a record from the database given a patient
     * id that exists in the database
     * @param id the patient Identity number
     */
    public void delete(String id){
        System.out.println("delete method has been called");
        System.out.println(" The key is " + id);
        try{         
        Connection conn = null; 
        Class.forName("com.mysql.jdbc.Driver").newInstance(); 
        conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/hospital?user=root&password="); 
        PreparedStatement p =conn.prepareStatement("delete from patient where patientID = ?");
        p.setString(1, id);
        p.execute();
        }
        catch(Exception e){
         System.out.println("Error"+e.toString());
        }
    }
    
    /**
     * a method that inserts / add a new record of a patient(s) to the 
     * database
     * @param id the patient identity as given to the user 
     * @param sur the sur name of the patient
     * @param first the first name of the patient
     * @param add the address of the patient
     * @param phone the phone number of the patient
     * @param next the next of Kin to the patient
     * @param cont the contact of the next of kin
     */
    public void insert(String id,String sur, String first,String add,String phone,String next, String cont){
        System.out.println("Inserting ...... ");
            try{
                Connection conn = null; 
                Class.forName("com.mysql.jdbc.Driver").newInstance(); 
                conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/hospital?user=root&password=");
                
                PreparedStatement p=conn.prepareStatement("INSERT INTO patient SET patientID = ?, surName = ?, firstName = ?, address = ?, phone = ?, nextOfKin = ?, contact = ?");
                p.setString(1, id);
                p.setString(2, sur);
                p.setString(3, first);
                p.setString(4, add);
                p.setString(5, phone);
                p.setString(6, next);
                p.setString(7, cont);
                p.execute(); //use execute if no results expected back
                }
            catch(Exception e){
                System.out.println("Error"+e.toString());
            }
        }
    
    /**
     * a method that imports / reads records from the database
     */
    public  void importData(){
        System.out.println("importing ... ");
        try {
            Connection conn = null; 
            Class.forName("com.mysql.jdbc.Driver").newInstance(); 
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/hospital?user=root&password="); 
            Statement s = conn.createStatement(); 
            ResultSet rs = s.executeQuery("SELECT * FROM patient"); 
            ResultSetMetaData meta = rs.getMetaData(); 
            int numberOfColumns = meta.getColumnCount(); 
            columnNames= new String[numberOfColumns]; 
            for (int i = 1; i <= numberOfColumns; i++) {  
               columnNames[i - 1] = meta.getColumnName(i);  
            } 
            System.out.println("PATIENT ID:"+"   \t   "+"SURNAME:"+"   \t   "+"FIRSTNAME:"+"   \t   "+
                    "ADDRESS:" +"   \t   "+"PHONE:"+"   \t   "+"NEXT OF KIN:"+"   \t   "+"CONTACT:");
            
            while(rs.next()) {
                 System.out.println("");
                System.out.println(rs.getString("patientID")+" \t  \t   "+ rs.getString("surName")+" \t     " +rs.getString("firstName")+" \t  \t   "+rs.getString("address")+" \t     "+rs.getString("phone")+" \t  \t   "+rs.getString("nextOfKin")+" \t     "+rs.getString("contact")+"   \t   ");
            }

        }
    catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            System.exit(0);
       } 
    }
    
    /**
     * a method that exports information to a text editor
     */
    public void exportData(){
        System.out.println("exporting ...");
        try{
            Connection conn = null; 
            Class.forName("com.mysql.jdbc.Driver").newInstance(); 
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/hospital?user=root&password=");
            
            PreparedStatement p = conn.prepareStatement("SELECT * FROM patient");
             
            ResultSet q=p.executeQuery();
            String style="%-20s %-20s %-20s %-20s %-20s %-20s %-20s%n";
           
            PrintWriter pw=new PrintWriter(new BufferedWriter(new FileWriter("patient.txt")));
            pw.write(String.format(style,"patientID", "surName","firstName", "address","phone","nextOfKin","contact"));
            while(q.next())
               
              
            pw.write(String.format(style ,q.getString("patientID"),q.getString("surName"),q.getString("firstName"),q.getString("address"),q.getString("phone"),q.getString("nextOfKin"),q.getString("contact")));
            pw.close();
            
           }catch (Exception e){
              System.out.println(e);
              System.exit(0);
          }
        }
}
